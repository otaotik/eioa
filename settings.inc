%============%
%== LAYOUT ==%
%============%
%== COLORS ==%
\ifprintable
\definecolor{clrbck}{rgb}{1,1,1}
\definecolor{clrbcktit}{rgb}{1,1,1}
\definecolor{clrfg}{rgb}{0,0,0}
\newcommand{\clrfg}{\color[rgb]{0.7,0,0}}
\newcommand{\clrmain}{000000}
\newcommand{\clrgrey}{808080}
\newcommand{\clrsec}{000000}
\newcommand{\clrseclight}{000000}
\newcommand{\clrpart}{B20000}
\else 
\definecolor{clrbck}{rgb}{0.97,0.92,0.75}
\definecolor{clrbcktit}{rgb}{0.92,0.85,0.60}
\definecolor{clrfg}{rgb}{0.5,0.2,0}
\newcommand{\clrfg}{\color[rgb]{0.5,0.2,0}}
\newcommand{\clrmain}{602000}
\newcommand{\clrgrey}{602000}
\newcommand{\clrsec}{902000}
\newcommand{\clrseclight}{C07055}
\newcommand{\clrpart}{602000}
\fi

%== HEADER AND FOOTER ==%
\clearpairofpagestyles
\ohead{%
\thepage
}
\cfoot{%
\psvectorian[height=0.7\footheight, color=clrfg]{75}%
}

%== BACKGROUND COLOR ==%
\ifprintable
\else
\AddToShipoutPictureBG{\begin{tikzpicture}[remember picture,overlay]
 \path [left color = clrbck, right color = clrbck]
 (current page.south west)rectangle (current page.north east);   % Adjust the position of the logo.
\end{tikzpicture}}
\fi

%===========%
%== FONTS ==%
%===========%
%== MAIN FONT ==%
\setmainfont{EB Garamond}[%
    Color=\clrmain,
    SmallCapsFont={EB Garamond SC},
    SmallCapsFeatures={Letters=SmallCaps},
    Ligatures={Common,Rare}
]
\setsansfont{Ubuntu Light}

%== LETTRINES ==%
\newfontface{\initials}{EB Garamond Initials}
\setcounter{DefaultLines}{4}
\renewcommand{\DefaultLraise}{0.25}
\renewcommand{\DefaultLoversize}{-0.25}
\setlength{\DefaultNindent}{0.2em}
\renewcommand{\LettrineFontHook}{\initials}
\newcommand{\clettrine}[2]{%
\lettrine{\clrfg #1}{#2}%
}

%== SECTIONING FONTS ==%
\setkomafont{partnumber}{\Huge \addfontfeature{Color=\clrpart} \centering}
\setkomafont{part}{\Huge \addfontfeature{Color=\clrsec} \centering}
\setkomafont{chapter}{\Huge \addfontfeature{Color=\clrsec} \scshape \centering}
\setkomafont{section}{\LARGE \addfontfeature{Color=\clrsec} \scshape \centering}
\setkomafont{subsection}{\Large \addfontfeature{Color=\clrsec} \centering}
\setkomafont{subsubsection}{\normalsize \addfontfeature{Color=\clrsec}}
\setkomafont{disposition}{\rmfamily}

%== SECTIONING ADJUSTMENTS ==%
\newcommand{\parsection}[1]{{\addfontfeature{Color=\clrsec}\emph{#1}}} %== Most important word of paragraph can be highlighted
\newcommand{\nsection}[1]{\needspace{15cm}%== We have nicer sections which need wrap-up command
\section*{%
\addcontentsline{toc}{section}{#1}%
#1%
}}
\newcommand{\nsubsection}[1]{%== We have nicer subsections which need wrap-up command
\subsection*{%
\addcontentsline{toc}{subsection}{#1}%
#1%
}
}
\renewcommand{\raggedchapter}{\centering}
\newcommand{\nchapter}[1]{\chapter*{%== We add some shit to chapters as well
\addcontentsline{toc}{chapter}{#1}%
\psvectorian[height=0.6\baselineskip, color=clrfg]{72}%
#1%
\psvectorian[height=0.6\baselineskip, color=clrfg]{73}%
}}
\renewcommand\partheadmidvskip{%== We split the chapter number and name by an ornament
  \par\nobreak\vspace*{\baselineskip}%
  \psvectorian[width=7cm]{88}%
  \par\nobreak\vspace*{\baselineskip}%
}

\newcommand{\introtext}[4]{%== After a section we can have optional quotes
\begin{center}
\begin{tikzpicture}[every node/.style={inner sep=0pt}]   
\node[text width=10cm,align=justify](Text){%
\parindent=1em
\itshape
#1
\vspace{\baselineskip}
};
\node[shift={(1cm,-\baselineskip)},anchor=south east](author)  at (Text.south east){%
 --- #4
};
\node[shift={(1cm,-2.0\baselineskip)},anchor=south east](author)  at (Text.south east){%
 #2 (#3)
};
\end{tikzpicture}%
\end{center}%
}

\ifincluderules
\newcommand{\ruleblock}[1]{\textsf{\small #1}}
\else
\newcommand{\ruleblock}[1]{}
\fi