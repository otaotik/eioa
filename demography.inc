\part{Demografie}
\nchapter{Rasy}
\nsection{Lidé}
\introtext{%
"Je to jednoduché – chodí sem totiž lidi.
A to znamená, že se může stát cokoliv."
}{%
Drž mě pevně, miluj mě zlehka
}{%
4e180
}{%
napsal \emph{Robert Fulghum}
}

\introtext{%
Všechen ten spěch, ambice dosáhnout velkých věcí než jejich krátké životy odvane vítr
  --- jak marné vypadá jejich pachtění.
Ale pak spatře plody jejich snažení, musíš ocenit jejich úspěchy.
Kéž by na pár chvil mohli zpomalit a zušlechtit své konání.
}{%
Elfí kroniky
}{%
4e132
}{%
napsal neznámý autor
}

\clettrine{L}{idé} jsou proslulí svou přispůsobivostí a schopností přežít i v těch nejnehostinějších krajích.
Z toho důvodu je nelehký úkol popsat typického člověka --- takový neexistuje.
Jedinci jsou vysocí od pouhých 5 stop až po 7 stop mezi obyvateli severských plání.
Najdeme mezi nimi velké myslitele, i dobré válečníky.
Je až s podivem, kolik toho člověk za svůj krátký život stihne.
Říká se, že ať umíš cokoliv, najdeš lidského muže, či ženu, který umí totéž.

\nsection{Elfové}
\introtext{%
  Každý den se do lesa vracel s dary.
  Dlouhé hodiny plakal a prosil, aby jej vzali mezi sebe.
  Nevěděl, zdali jej poslouchají, či ne.
  Ale nikdy se nevzdal.
  Až se, po několika letech, opravdu dočkal.
  Najednou cítil jejich blízkost, a přestože nikoho neviděl, ihned věděl, že jsou všude kolem něj.

  \uv{Chceš být jedním z nás?} zeptali se ho.

  \uv{Ano.}

  No a tak ho uvařili a snědli
  }{%
  Zlidovělá pohádka z období válek
  }{%
  4e17
  }{%
  napsal \emph{Persimus Philt}
}

\clettrine{A}{znešení}

\nsubsection{Lesní elfové}
Obývajíc plodné džungle elfie, přispůsobili se lesní elfové životu v lese.
Abnormálně ostré smysly je chrání před divočejšími obyvateli džunglí --- hady, šelmami, jedovatými pavouky \ldots
A před útočníky, které nelze vidět, je chrání vrozená odolnost vůči jedům a nemocem.
Tyto vlastnosti společně s elfí dlouhověkostí pomáhají udržet populaci lesních elfů na vysokém počtu.
Jsou to jen nízké ambice a neochota opouštět domovinu, která zabraňuje lesním elfům šířit se po celé Eioae.

Mírumilovní a přátelští, vítají lesní elfové většinu návštěvníků s nehranou pohostinností a vřelostí.
Dokud se ovšem tito návštěvníci drží míst jim určených.
Pokud by se tito odvážili vstoupit do hlubších džunglí skrývajících elfí svatyně a domovy starších elfů, pravděpodobně by se již nevrátili o tom vyprávět.
\nsubsection{Vznešení elfové}
Né všichni elfové se spokojí se životem v hlubokých džunglích.
Někteří dávají přednost jasnému slunci a výhledu do širokých krajů.
Dají přednost luxusu a blahobytu před ojídáním kořínků.
Tito kdysi dávno opustili své rodné džungle a vydali se hledat štěstí na lidmi obývané Eule.
Zde po dlouhých bojích zvládli lidi vytlačit až k hradbám Belana, města na úpatí hor.

Pohrdajíc všemi ostatními a považujíc sebe za jediné pravé obyvatele Eioy nazvali se tito elfové vznešenými.
O tomto označení ovšem ostatní rasy, a převážně lidé, pochybují.
\nsubsection{Drouové}
Když se vznešeným elfům narodí nedokonalé --- dle jejich vysokých požadavků nedokonalé --- dítě, je to velké neštěstí.
Takové dítě by brzy pokazilo celé pokolení a je tedy obtíží, které je nutné se zbavit.
Po dovršení elfí dospělosti, třiatřicátého roku života, jsou tyto děti bez zlatavých vlasů a bez zlatavé kůže bez okolků vyhnáni.

Tito vyhnanci poté prchají směrem k horám a využívají bezpečí obrovských jeskyních komplexů skrývajících se pod těmito horami.
Životu v podzemí se zcela přispůsobili, a leč nejsou tak krásní jako jejich vznešení bratři, jsou mezi ostatními většinou oblíbenější.
Nalezli přirozené spojenectví s lidmi a udržují čilé styky s Belanem, obchodníkům odkud pomáhají rozvážet zboží podzemínimi tunely pod horami.
\nsubsection{Píseční elfové}

\nsection{Trpaslíci}
\introtext{%
O trpaslících je všeobecně známé, že jsou to svině chamtivé.
Ale na jejich slovo se můžeš spolehnout.
}{%
Zápisky pro syna
}{%
4e78
}{%
napsal kupec Belánský
}
\clettrine{T}{rpaslíci} jsou pevní jako skály, ve kterých žijí.

\nsubsection{Severští trpaslíci}
Osídlit mrazivé pláně Norby, kde v noci teploty klesají až k mínus třiceti stupňům není snadný úkol.
Zvládli ho jen severské kmeny lidí a severští trpaslíci.
Jestli jsou běžní trpaslíci odolní, jsou ti severští nezničitelní.
Zvyklí přežít dlouhá období mrazů, bez pořádného jídla, a bez odpočinku, posilněni pouze lahví ledového vína, je pro ně libovolný neduh počasí obyvatelů příjemnějších biomů důvodem k bodrému smíchu a veselí.

I přes jejich tvrdost získanou životem v tak nehostiném prostředí jsou severští trpaslíci velice přátelští.
Málokdy na daleký sever přijíždější kupecké lodě, či dokonce dobrodružnější teplouši, jak bodře říkají teplomilnějším rasám, z jihu.
Tedy je taková návštěva vždy velice vítaná, neboť přiváží vzácné zboží a ještě vzácnější novinky ze světa.

Na dalekém severu odvykli severští trpaslíci válkám.
A ti bojechtivější většinou cestují na jihozápad do divočiny, kde se začleňují do sídel běžných trpaslíků.

\ruleblock{
  Tihle týpci mají prostě mega odolnost +2 oproti normálním trpaslíkům.
  Ale mají zamrzlé kosti, takže mají taky -2 obratnost.
  Nevadí jim mráz ani mrazivá kouzla.
  Jsou zvyklí na alkohol a vydrží ho fakt hodně.
}
\nsubsection{Sopeční trpaslíci}
\nsubsection{Divocí trpaslíci}

\nsection{Gnómové}
\nsection{Pulčíci}
\introtext{
    Když si ve stresu, vyčiluj se.
}{%
}{%
???
}{%
staré hobití přísloví
}
\nsection{Vouzové}
\nsection{Keťáci}
\nsection{Zelení}
\nsubsection{Kultivování orkové}
\nsubsection{Divocí orkové}
\nsubsection{Skřeti}
\nchapter{Politika}
\nsection{Gora}
\nsection{Elfia}
\nsection{Kopi}
\nchapter{Komunity}
\nsection{Belano}
\clettrine{B}{elano} nejbohatší město kontinentu Gory leží v deltě řeky Kimše.
