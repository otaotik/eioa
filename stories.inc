\part{Příběhy}
\nchapter{Genesis}
\nsection{První éra}
\introtext{%
\ldots spatřil jsem hořící vítr barev, pulzující plameny duhy.
Jako ptáče při svém prvním letu cítil jsem radost i strach z neznámého.
Nerozuměl jsem ničemu, a přesto jsem věděl, že je vše jak má být.
A pak rázem bylo vše špatně \ldots%
}{%
Stvoření
}{%
4e27
}{%
napsal \emph{Itaer Eleno}
}

\clettrine{N}{ikdo}, ani oni sami, netuší, odkud se vzali.
% Dle jejich paměti existují již od nepaměti. 
Chyceni ve věčném konfliktu, který udržují spíše už jen ze setrvačnosti, neboť důvody k němu si již nepamatují.
Tehdy existovali pouze oni.
Devět bohů, kteří si sami byli dimenzí i esencí a sami sebe definovali.
Nic jiného nebylo.

\parsection{Demius} je pozadím na kterém se vše odehrává, prostorem, kde se muže rozprostírat hmota, časem, ve kterém se vše děje. 
\marginpar{\footnotesize 
Tady je jenom nějaký droboučký textík.
Protože ten dlouhý pak udělá problémy.
A my to chceme jen vyzkoušet.
}
Je protikladem, který pomáhá definovat věci.
Má se k existenci jako se má tma ke světlu a ticho ke zvuku. 
Nebýt jeho, nemohlo by být nic jiného ale sám o sobě není ničím.
Nemá přílišné ambice a je vždy spokojený s tím, co je jsoucí, neboť to mu udává podobu a myšlenku. 
Považuje se za nejduležitějšího ze všech bohů a málokterý to zpochybňuje, neboť vědí, že bez něj by nebyli.

\parsection{Cranis} je myšlenkou. 
Duvodem všeho. 
Když se ptáme: 
“Proč jsme tady? Proč existujeme?” 
odpovědí je Cranis. 
Podobně jako Demius ani on sám nemá žádné skutečné tělo, žádnou podobu. 
Jeho doménou jsou myšlenky tvoru a v těch se nachází a rozlézá. 
Néjenom že ví, co si kdo myslí, ale sám tou myšlenou věcí je.
Sic je odpovědí na otázku existence a bytí, sám přemýšlí nad odpovědí na otázku mnohem složitější, která ho samotného děsí. 
Jedinou otázku bez odpovědi. 
Jestliže je myšlenkou, jak on sám muže myslet?

\parsection{Osud} je vývojem událostí. 
Zatímco Cranis jsou myšlenky v danou chvíli, Osud ví, jaké myšlenky byly v minulosti a jaké budou v budoucnosti. 
Podle něj se vše vyvíjí od nekonečna do nekonečna a Osud zná každý jednotlivý detail každého okamžiku celého jsoucna. 
Je tím nejděsivějším bohem ze všech, neboť je to právě on, kterého se ostatní bohové bojí. 
Zná detaily jejich životu budoucích a jejich smrti.
On sám zná detaily své vlastní smrti a dělá vše ve svých silách, aby jí zvrátil. 
Ale sám nejlépe ví, že se mu to nemuže podařit. 
Musel by zničit sám sebe a tím by jen navršil svuj osud.

\parsection{Meba} je řád. 
Myšlenky jsou krásná věc, ale abychom je chápali a mohli s nimi pracovat, musíme je umět uspořádat. 
Jsoucno se muže vyvíjet samo o sobě, ale bez řádu a uspořádání je všem k ničemu. 
Když se díváme na šnečí ulitu, nevidíme spirálu. 
Vidíme Mebu. 
Meba je prvním z jmenovaných, který nabývá nějaké podoby. 
Meba je fraktálem a dívat se za ní znamená propadat se do nekončících obrazu se stále většími detaily.
Miluje symetrii a kružnice. 
Ostatní bohové ji respektují, neboť je vždy spolehlivá.

\parsection{Romban} je protikladem Meby. 
Chaos, oheň. 
Je si zcela vědom, že je definován Mebou skrze Demiuse, a připadá si druhotný, nepodstatný. 
Jeho cílem je vše pohltit ve svých plamenech. 
Vždy září jasně a nehlídán se šíří všude, kde je pro něj prostor. 
Je maximální entropií. 
Snad svou záští ke svým bratřím, snad je to přímo jeho podstata, touží zničit vše. 
Nejen materiálno a dimenzno, ale převážně vztahy a ostatní bohy.
Libuje si v konfliktech.

\parsection{Rangal} je dítětem zrozeným z konfliktu Rombana a Meby. 
Neustálý konflikt chaosu a řádu, tvoření a ničení, rustu a dekompozice my známe jako život. 
Rangal je právě životem. 
Energií, která nás všechny prostupuje, radostí, že jsme zde. 
Jako je Cranis duvodem, proč jsme zde, je Rangal duvodem proč zde být i nadále. 
Rangal má podobu smíchu, když se smějeme s přáteli, které máme rádi.

\parsection{Eliah}, známá také jako Touha, je personifikací našeho nejtajnějšího snu. 
Ta dívka, ten chlapec, o kterých se nám zdají ty nejromantičtější sny. 
Ty v ní vidíme, cítíme. 
Nebo v něm, záleží, kdo se dívá. 
Eliah jsou dva milenci v lese.
Ale není jenom krásná. 
Je také tam, kde manžel zabíjí manželku s milencem. 
Tam kde si mladá dívka se zlomeným srdcem podřezává žíly. 
Je příčinou každého znásilnění. 
Je spalující závistí. 
Milována všemi, nemiluje nikoho.
Voní po lesních jahodách. 
Jestli existuje pravá láska, je to láska k ní.

\parsection{Bose} je síla. 
Drží věci pohromadě a drží věci od sebe. 
Bez něj by nebylo pohybu a možnost manipulace. 
Skrz něj se ostatní bohové mohou realizovat. 
Touha bez síly pro ní cílit je jen malým plamínkem, který brzy pohasne. 
Život je boj a na boj potřebujeme sílu.
Neboť je Bose interakcemi je on ten, kdo nám dává smysly. Díky němu vidíme, neboť je paprsky dopadajícími na sítnici, slyšíme, neboť jsou to jeho prsty, které brnkají na struny v našich uších.
Sic tedy nemá vlastní podobu, vnímáme ho vždy a všude. Ve všem.

Poslední z nich, \parsection{Adus}, je tam, kde ostatní nejsou.
Nemá žádnou viditelnou formu. 
Naopak je temnotou. 
Dívat se na něj znamená nevidět nic než nejčernější čerň. 
Je nepřítomností zvuku, nelze jej slyšet. 
Bez chuti, bez zápachu. 
Dotknout se ho je dotykem prázdnoty, která mrazí na duši a dává člověku pocit, že už nikdy nepocítí něčí dotek. 
Pomyslet na něj znamená nemyslet na nic. 
Nejblíže jsme mu, když jsme sami, opuštění. 
Smutek, který cítíme je nejspíše podvědomý prastarý strach z něj. 
Až jednou veškeré jsoucno skončí zbyde jenom Adus. 
Adus je Neexistence. 

Jak to tak bývá, všichni myslí především na sebe. 
Lidi, elfové, orkové, elementálové, dobří duchové, démoni i andělé. 
Ani abstraktní koncepty které utváří existenci nejsou výjimkou. 
A tak spolu těchto devět bohů začlo soupeřit o nadvládu jeden nad druhým a nad svým společným jsoucnem, které nyní nazýváme Paal. 
Bohové neměřili čas, neboť čas je jedním z nich, tedy konflikt trval pouhý zlomek vteřiny a trval i nekonečně dlouho. 
Jeden druhého zrazoval snad častěji než s někým spolupracoval.

Jeden z nich byl mocnější než všichni ostatní. 
Byl vždy o krok napřed a jehož bytí znemožňovalo, aby prapůvodní válka kdy skončila. 
Byl to Osud, a ostatní bohové si uvědomovali, že dokud bude žít, nebudou schopni se vymanit z cest, které on dávno určil.
Tak se stalo, že Osud byl tím důvodem, proč se osm ostatních bohů spolčilo. 
Je ironií osudu, že ten, kdo znal a kontroloval osudy všech, nemohl změnit osud svůj. 
Společně bohové zvládli Osud přemoci a zničit. 
Nejspíše to tak ovšem chtěl, neboť věděl, že se tím osvobodí. 
A osvobodí i své bratry.

S jeho smrtí však cítili, že ztratili kus sebe sama. 
Ztratili svého nejstaršího bratra, který vždy kontroloval jejich životy a držel nad nimi ochrannou ruku. 
K vlastnímu žalu tomu porozuměli až nyní, když už není cesty zpět.
Tato tragédie přinutila zbývajících osm bohů změnit svůj náhled na vše a přestat mezi sebou soupeřit. 
Utvořili jednotnou entitu a nazvali se Paal. 
Břímě války se rozhodli přesunout na jiné. 
A tak Paal stvořili svět, kde spolu budou válčit smrtelníci.

\nsection{Druhá éra}
\introtext{%
Ono ten svět byl takový nejprve celý hořlavý a tekutý.
Inu láva to byla.
A všude to bouchalo a třaskalo;
A vířilo a bouřilo.
To tam tehdá ještě nikdo nežil.
Živí tvoři potřebují trošku jiné životní podmínky, víme?
}{%
Hospodské vyprávění
}{%
4e157
}{%
vyprávěl \emph{Ulfric Skálopal}
}
\clettrine{T}{oliko}%
\lipsum

\nsection{Třetí éra}
\introtext{%
Martin se zahleděl v dál a znovu se rozplakal.
Puchýře na rukou ani necítil, neboť větší bolest mu způsobovaly rány na duši.
Své starší bratry pohřbil již před léty.
Nyní pohřbil Matouše, svého posledního, mladšího, bratra, který ve válce zemřel ve věku nedožitých 11 let.

Boží válka byla totiž neúprosná.
Den za dnem na frontách umírali bojovníci po stovkách.
Martin věděl, že jsou jen figurky bohů, odsouzení jim sloužit v jejich hře.
Obličej se mu zkřivil vztekem a tělo se mu roztřáslo.
Přísahal bohům pomstu a rozhodl se konat hned.
}{%
Legenda o Martinovi
}{%
4e60
}{%
napsal \emph{Rudolf z Prasečího letohrádku}
}

\nsection{Čtvrtá éra}
\introtext{%
Zbavili jsme se svých trýznitelů, jen abychom zjistili, že sami si dokážeme ublížit mnohem víc.
}{%
Projev při začátku elfích válek
}{%
4e15
}{%
řekl \emph{Martin III. král Gerie}
}