\part{Magika \& Technika}
\nchapter{Magika}
\nchapter{Technika}
\nsection{Střelný prach}
\introtext{%
Zástupy elfů v lesklých zbrojích rychle postupovaly kupředu, nehledíce na neúprosný žár slunce Chalidské pouště.
Možná i tušili, že se k branám Alchíru dostali příliš snadno, ale pýcha jim nedovolila zpomalit.
\ldots
Náhle se ozvala ohlušující rána jako když udeří milión blesků.
Vzduch všude kolem se náhle proměnil v plameny Hélosu.
Okolní domy se začaly rozpadat a elfí průvod zasypaly bortící se zdi.
Vzduchem lítaly hořící kameny
\ldots
Těch pár přeživších bylo po zbytek svých životů pronásledováno nářkem umírajících a zápachem spáleného masa.
}{%
Kronika rasových válek
}{%
4e64
}{%
napsal \emph{Vido Bodřič}
}
\clettrine{D}{o} hlavně je udělán vývrt se dvěma druhy prvků, těmito prvky jsou drážky a pole.
Drážky jsou místa, která jsou "vyříznuta" v hlavni a vystouplé prvky se nazývají pole.
Drážky a pole se mohou lišit počtem, hloubkou, tvarem, směrem stoupání (pravo a levotočivé) a velikostí stoupání.
Rotace udělená vývrtem projektilu výrazně zvyšuje jeho stabilitu, tím zvětšuje dostřel a zlepšuje přesnost.

Vývrt hlavně lze charakterizovat jeho stoupáním, to nám říká, že na jednu otáčku musí projektil urazit danou vzdálenost.
Např.: 1 otáčka na 500 milimetrů (1:500 mm).
Čím kratší je vzdálenost, na které se projektil otočí, tím větší musí být stoupání vývrtu a tedy i rotace projektilu.
\marginpar{\footnotesize 
Vlastnost \emph{dlouhé nabíjení} zabraňuje nositeli vystřelit více než jednou za kolo. Mezi dvěma výstřely zbraně navíc vyžaduje nabití, které spotřebuje 1 akci.

\emph{Rychlý prohoz} umožňuje nositeli jako volnou akci prohodit v ruce danou zbraň za libovolnou jinou.
}
\begin{center}
    %\small
    \centering
    \begin{tabular}{lrrrl}
    \multicolumn{1}{l}{\itshape{Zbraň}} &%
    \multicolumn{1}{r}{\itshape{Cena}} &%
    \multicolumn{1}{r}{\itshape{Zranění}} &%
    \multicolumn{1}{r}{\itshape{Váha}} &%
    \multicolumn{1}{c}{\itshape{Vlastnosti}} \\[5pt]
    Píšťala & 70z & 1k8 bod & 2lb. & náboje (40/160)\\
    & & & & dlouhé nabíjení\\
    & & & & rychlý prohoz \\[5pt]
    Mušketa & 120z & 1k10 bod & 5lb. & náboje (60/240)\\
    & & & & dlouhé nabíjení\\
    & & & & dvouruční \\[5pt]
    Vývrtka & >1000z & 1k12 bod & 5lb. & náboje (200/600)\\
    & & & & dlouhé nabíjení\\
    & & & & dvouruční \\
    \end{tabular}
\end{center}
\psvectorian[height=0.5\baselineskip]{21}
Zbraň s vlastností \emph{dlouhé nabíjení} nemůže vystřelit více než jednou za kolo. Mezi dvěma výstřely navíc vyžaduje nabití, které vyžaduje 1 akci.
\psvectorian[height=0.5\baselineskip]{23}

\psvectorian[height=0.5\baselineskip]{21}
Zbraň s vlastností \emph{rychlý prohoz} lze jako volnou akci ukrýt do pouzdra a vyměnit v ruce za jinou zbraň.
\psvectorian[height=0.5\baselineskip]{23}
\vspace{\baselineskip}

Muškety byly velkorážové zbraně, do kterých byly používány těžké a "pomalé" kule.
Tyto zbraně neměly vývrt, respektive měly hladký vývrt hlavně.
Při výstřelu docházelo vlivem nedostatečného vedení kule k jejímu poskakování uvnitř hlavně, to vedlo k nepřesnosti střelby.
Bylo zapotřebí vyrábět mnohem přesnější hlavně (minimalizovat vůli mezi hlavní a kulí), to však bylo technologicky velmi náročné a nákladné.
