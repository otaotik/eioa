\part{Geografie}
\nchapter{Eioa}
Dle výpočtů gnómských astronomů má svět Eioy tvar tóru na kterém známé kontinenty zabírají jen drobnou část.
Podezřívají tedy, že existují další dosud neobjevené kontinenty daleko za oceány.
Možná proto existují legendy o prapodivných rohatých lidech, kteří občas připlují na obrovských okřídlených lodích a vyprávějí o krajích a divech všem neznámým.
\nchapter{Frelso}
Tak místní říkají jim známému světu.
\nsection{Eula}
Východní kontinent mírného klimatu.
Západní část od Belána k popřeží patří hobitům, kteří žijí poklidný farmářský život přerušovaný občasnými nájezdy seveřanů.
Jížní část patří vysokým elfům.
Východní část obývají gnomové.
Severní bažinatá část patří upírům, vlkodlakům a dalším děsivým stvořením, která si podmanila místní lidské obyvatele.
V prostřed kontinentu je obrovské pohoří na jehož úpatí leží město Belano.
Pod pohořím jsou rozlehlé jeskynní komplexy propojené tunely.
Ty jsou obývány Vouzy a Temnými elfy.
\nsection{Tunra}
Západní, pouštní, kontinent.
Severovýchodní výběžek, oddělený pásem Chalidské pouště, má velice příjemné klima vhodné pro pěstování téměr všech plodin.
Tento výběžek, nazývaný Porpaňa, je obýván zástupci všech ras.
Severozápad obývají orkové, kteří jsou v neustálém konfliktu s obyvateli jižní části --- Keťáky a písečními elfy.
\nsection{Elfasie}
Jižní ostrůvky plné džunglí.
Domov lesních elfů.
\nsection{Norba}
Severní ledová země. Domov trpaslíků a severských lidských kmenů.
\nsection{Divočina}
Západní velký ostrov plný nebezpečných, obřích tvorů.